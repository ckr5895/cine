Consideraciones

*Para levantar base de datos
-php artisan migrate
-php artisan db:seed -> se generan algunos registros de prueba

Se crea usuario por defecto con las credenciales siguientes

email: admin@globalstd.com
password: password

*Correr comando php artisan passport:install y mover los datos generados al archivo .env de la siguiente manera
PASSPORT_LOGIN_ENDPOINT=http://cine.test/oauth/token -> url de la aplicación
PASSPORT_CLIENT_ID=2 -> este id será el mismo siempre
PASSPORT_CLIENT_SECRET=MIJfGKXZtt6UWmFZ1jqCjtRyKBzojGdjnBsoDqUB -> client secret del client con id 2


Se anexa un archivo sql en el directorio raiz, con la base de datos generada con lo anterior, en este caso el PASSPORT_CLIENT_SECRET sera igual a cw59J2l37xkugvgxbzOL4MaLA0UHWz9i6PKgDFzI