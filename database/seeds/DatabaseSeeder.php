<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => "Administrador",
        	'email' => "admin@globalstd.com",
        	'email_verified_at' => now(),
        	'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        	'remember_token' => Str::random(10),
        ]);
        factory(App\Turno::class,15)->create();
        factory(App\Pelicula::class,10)->create();
    }
}
