<?php
namespace App\Classes\Errores;

class SQL {
    public static function sql($codigo){
        switch ($codigo) {
        	case '23503':
        		return "Hay datos que tienen relación con este registro, por lo cual no es posible eliminarlo en este momento.";
        		break;

        	default:
        		return "Hay un error no identificado, el código del error es: ".$codigo.". Por favor guarde el código para reportar el error.";
        		break;
        }
   }
}
