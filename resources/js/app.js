/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
window.toastr = require('toastr')
window.swal = require('sweetalert2');
import { ModalPlugin } from 'bootstrap-vue'
import VueRouter from 'vue-router';
import router from './router';
import store from './store';
import vueDropzone from "vue2-dropzone";
import 'vue2-dropzone/dist/vue2Dropzone.min.css'

toastr.options = { "positionClass": "toast-bottom-right"}
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('app', require('./App.vue').default);
Vue.component('vue-dropzone', vueDropzone);
Vue.use(VueRouter);
Vue.use(ModalPlugin)
//Se valida si se ha iniciado sesión, de lo contrario se redirecciona a la ruta login
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.loggedIn) {
        next({
        name: 'login',
        })
    } else {
        next()
    }
    } else {
    next() // make sure to always call next()!
    }
});

//Filtro para obtener imágenes del servidor y no poner toda la ruta en el componente
Vue.filter("imagenPerfil",  (value) =>{
  if(value.split(',').length>1)
    return value;
  else
    return "/images/users/"+value;
});


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store
});
