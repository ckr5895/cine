<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    protected $fillable = ['nombre','fecha_publicacion','imagen','activo'];

    public function turnos(){
      return $this->belongsToMany('App\Turno');
    }
}
