<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeliculaTurnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelicula_turno', function (Blueprint $table) {
            $table->integer("pelicula_id")->unsigned();
            $table->integer("turno_id")->unsigned();
            $table->foreign("pelicula_id")->references("id")->on("peliculas");
            $table->foreign("turno_id")->references("id")->on("turnos");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelicula_turno');
    }
}
