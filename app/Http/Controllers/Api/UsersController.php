<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Classes\Tools\Imagen;
use App\User;

class UsersController extends Controller
{
    public function update(UserRequest $request,$id){
    	$user = User::findOrFail($id);

    	$user = User::findOrFail($id);
        $foto_anterior=$user->foto;
        $user->fill($request->all());
        if($request->foto){
            $user->foto = Imagen::procesarImagen($user->foto,"users",$foto_anterior,true);
        }
        $user->save();
        return response()->json($user);
    }
}
