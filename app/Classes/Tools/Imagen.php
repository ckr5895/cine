<?php
namespace App\Classes\Tools;

use Illuminate\Support\Str;

class Imagen {
    public static function procesarImagen($nueva_imagen,$folder,$anterior_imagen = null,$isUpdate = false)
    {

      if(!empty($nueva_imagen)){
        if($anterior_imagen!=$nueva_imagen){
          if($isUpdate && !empty($anterior_imagen)){
            if(file_exists(public_path("images/$folder/".$anterior_imagen))){
              unlink(public_path("images/$folder/".$anterior_imagen));
            }
          }
          $exploded =explode(',',$nueva_imagen);
          $decoded=base64_decode($exploded[1]);

          if(str_contains($exploded[0],'jpeg'))
            $extension='jpg';
          else
            $extension='png';

          $nombre_archivo=Str::random().'.'.$extension;

          $path=public_path()."/images/$folder/".$nombre_archivo;
          file_put_contents($path,$decoded);

          return $nombre_archivo;
        }
        return $nueva_imagen;
      }
      return null;
  }
}