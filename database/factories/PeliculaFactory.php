<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pelicula;
use Faker\Generator as Faker;

$factory->define(Pelicula::class, function (Faker $faker) {
    return [
        "nombre" => $faker->text(50),
        "fecha_publicacion" => $faker->date("Y-m-d"),
        "activo" => $faker->boolean
    ];
});
