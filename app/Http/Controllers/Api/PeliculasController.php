<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\PeliculaRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Errores\SQL;
use App\Classes\Tools\Imagen;
use App\Pelicula;
use DB;

class PeliculasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peliculas = Pelicula::get();
        return response()->json($peliculas);
    }

    public function turnos($id){
        $turnos = DB::table("pelicula_turno")->where('pelicula_id',$id)->pluck('turno_id');
        return response()->json($turnos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PeliculaRequest $request)
    {
        $pelicula = new Pelicula($request->all());
        $pelicula->imagen = Imagen::procesarImagen($pelicula->imagen,"peliculas");
        $pelicula->save();
        return response()->json($pelicula);
    }

    //Se recibe array de id's de turnos e id de película
    public function storeTurnos(Request $request,$id){
        $pelicula = Pelicula::findOrFail($id);
        $pelicula->turnos()->sync($request->turnos);

        return response()->json(['msj' => 'ok'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pelicula = Pelicula::findOrFail($id);
        return response()->json($pelicula);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PeliculaRequest $request, $id)
    {
        $pelicula = Pelicula::findOrFail($id);
        $foto_anterior=$pelicula->imagen;
        $pelicula->fill($request->all());
        if($request->imagen){
            $pelicula->imagen = Imagen::procesarImagen($request->imagen,"peliculas",$foto_anterior,true);
        }
        $pelicula->save();
        return response()->json($pelicula);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pelicula = Pelicula::findOrFail($id);
        try {
            $pelicula->turnos()->detach();
            $pelicula->delete();
            return response()->json($pelicula);
        } catch (\Illuminate\Database\QueryException $error) {
          return response()->json(['errors' => [SQL::sql($error->getCode())]],422);
        }
    }    
}
