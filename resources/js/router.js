import VueRouter from 'vue-router'
import NotFound from './components/errors/NotFound'
import Login from './components/auth/Login'

const router = new VueRouter({
    routes:[
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        { 
            path: '/404', 
            name: '404', 
            component: NotFound,
        },
        { 
            path: '*', 
            redirect: '/404', 
        },
        {
            path: '/', 
            component: require('./components/dashboard/index.vue').default,
            name: 'dashboard',
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/turnos', 
            component: require('./components/turnos/index.vue').default,
            name: 'turnos',
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/turnos/create', 
            component: require('./components/turnos/form.vue').default, 
            name: 'create_turno',
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/turnos/:id/edit', 
            component: require('./components/turnos/form.vue').default, 
            name: 'edit_turno',
            meta: {
                mode: 'edit',
                requiresAuth: true,
            }
        },
        {
            path: '/peliculas', 
            component: require('./components/peliculas/index.vue').default,
            name: 'peliculas',
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/peliculas/create', 
            component: require('./components/peliculas/form.vue').default, 
            name: 'create_pelicula',
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/peliculas/:id/edit', 
            component: require('./components/peliculas/form.vue').default, 
            name: 'edit_pelicula',
            meta: {
                mode: 'edit',
                requiresAuth: true,
            }
        },
        {
            path: '/perfil', 
            component: require('./components/perfil/index.vue').default,
            name: 'perfil',
            meta: {
                requiresAuth: true,
            }
        },
    ]
});

export default router;