import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    token: localStorage.getItem('access_token') || null,
    username: localStorage.getItem('username') || null,
    photo: localStorage.getItem('photo') || ""
  },
  getters: {
    loggedIn(state) {
      return state.token !== null;
    }
  },
  mutations: {
    retrieveToken(state, token) {
      state.token = token;
    },
    retrieveUsername(state, username) {
      state.username = username;
    },
    retrievePhoto(state, photo) {
      state.photo = photo;
    },
    destroyPhoto(state){
      state.photo = "";
    },
    destroyToken(state) {
      state.token = null;
    },
    destroyUsername(state){
      state.username = null;
    }
  },
  actions: {
    retrieveToken(context, credentials) {
      return new Promise((resolve, reject) => {
        axios.post('/api/login', {
          username: credentials.username,
          password: credentials.password,
        })
          .then(response => {
            const token = response.data.access_token
            localStorage.setItem('access_token', token)
            context.commit('retrieveToken', token)
            context.dispatch('retrieveUsername');
            resolve(response)
          })
          .catch(error => {
            console.log(error)
            reject(error)
          })
      })

    },
    retrieveUsername(context) {
        axios.get('/api/user', {
          headers: { Authorization: "Bearer " + context.state.token }
        })
          .then(response => {
            const username = response.data.name
            const photo = (response.data.foto) ? response.data.foto : "";
            localStorage.setItem('username', username)
            localStorage.setItem('photo', photo)
            context.commit('retrievePhoto', photo)
            context.commit('retrieveUsername', username)
          })
          .catch(error => {
            console.log(error)
          })
      // })

    },
    destroyToken(context) {
      
      if (context.getters.loggedIn){
        
        return new Promise((resolve, reject) => {
          axios.post('/api/logout', '', {
              headers: { Authorization: "Bearer " + context.state.token }
            })
            .then(response => {
              console.log(response)
              localStorage.removeItem('access_token')
              localStorage.removeItem('username')
              context.commit('destroyToken')
              context.commit('destroyUsername')
              context.commit('destroyPhoto')
  
              resolve(response)
            })
            .catch(error => {
              console.log(error)
              localStorage.removeItem('access_token')
              localStorage.removeItem('username')
              context.commit('destroyToken')
              context.commit('destroyUsername')
              context.commit('destroyPhoto')
              reject(error)
            })
        })

      }
    }
  }
})

export default store