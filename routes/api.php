<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Rutas de login y logout
Route::post('/login', 'AuthController@login');
Route::middleware('auth:api')->post('/logout', 'AuthController@logout');
//Rutas de recursos para la aplicación
Route::group(['middleware' => ['auth:api'],'namespace' => 'Api'], function () {
    Route::resource('turnos','TurnosController',['only' => ['index','store','show','update','destroy']]);
    Route::resource('peliculas','PeliculasController',['only' => ['index','store','show','update','destroy']]);
    Route::get('peliculas/{id}/turnos','PeliculasController@turnos');
    Route::post('peliculas/{id}','PeliculasController@storeTurnos');
    Route::put('users/{user}','UsersController@update');
});
