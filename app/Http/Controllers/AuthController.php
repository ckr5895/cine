<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    
    public function login(Request $request)
    {
        $http = new \GuzzleHttp\Client;
        try {
            
            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password,
                ]
            ]);
            return $response->getBody();

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            
            if ($e->getCode() === 400) {
                return response()->json('Solicitud incorrecta, por favor ingresa un usuario y contraseña válidos.', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json('Tus credenciales son incorrectas. Por favor intenta de nuevo', $e->getCode());
            }

            return response()->json('Algo salió mal en el servidor.', $e->getCode());
        }
    }
    
    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        
        return response()->json('Has cerrado sesión de forma correcta', 200);
    }
    
}
